N = 4000; 
fp = 1000; 
t = 0:1/fp:(N-1)/fp;

x = chirp(t,100,4,333); 
subplot(3,2,1)
plot(t,x);
xlabel('czas [s]');
ylabel('amplituda');

Nf = 4096; 
xf = fft(x,Nf); 

xf_mod = abs(xf); % widmo amplitudowe
f = linspace(0,fp/2,Nf/2+1); 
subplot(3,2,2)
plot(f, xf_mod(1:Nf/2+1));
xlabel('czestotliwosc [Hz]');
ylabel('modul widma');

M = 301; % dlugosc odpowiedzi impulsowej filtru
h = fir1(M-1,0.5); % wygenerowany filtr
th = 0:1/fp:(M-1)/fp; 
subplot(3,2,3)
plot(th,h);
xlabel('czas [s]');
ylabel('odp. impulsowa');

hf = fft(h,Nf); % modul transformaty dla filtra
hf_mod = abs(hf);
subplot(3,2,4)
plot(f,hf_mod(1:Nf/2+1));
xlabel('czestotliwosc [Hz]');
ylabel('modul transmitancji');

y = filter(h,1,x); % przepuszczenie sygnalu przez filtr
subplot(3,2,5)
plot(t,y)
xlabel('czas [s]');
ylabel('amplituda syg. po filtracji');

yf = fft(y,Nf); % sygnal po filtracji w dziedzinie czestotliwosci
yf_mod = abs(yf);
subplot(3,2,6)
plot(f,yf_mod(1:Nf/2+1));
xlabel('czestotliwosc [Hz]');
ylabel('modul widma syg. po filtracji');