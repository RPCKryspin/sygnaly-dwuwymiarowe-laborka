% skrypt wyznacza widmo amplitudowe i fazowe
% sygnalu bedacego suma dwoch sinusoid
clear all;
close all;

f1=80;	% czestotliwosc pierwszej sinosoidy
A1=1;		% amplituda pierwszej sinusoidy
phi1=0.4;	% faza pierwszej sinusoidy

f2=80.5;	% czestotliwosc drugiej sinosoidy
A2=2;		% amplituda drugiej sinusoidy
phi2=0;		% faza drugiej sinusoidy

N=400;		% dlugosc sygnalu
fp=200;		% czestotliwosc probkowania
Nf=N;     % d�ugo�� transformacji Fouriera
% -------------------------------------------------------------------
%       W tych warunkach odleg�o�� pomi�dzy pr��kami widma wynosi 1Hz
% -------------------------------------------------------------------

% generuj os czasu
t=0:1/fp:(N-1)/fp;
figure(1)
% generuj sygnal
x=A1*sin(2*pi*f1*t+phi1)+A2*sin(2*pi*f2*t+phi2);
subplot (311);
plot (t,x);
xlabel ('czas[s]');
ylabel ('sygnal');

% wyznacz widmo
widmo=fft(x, Nf) / Nf;
f = linspace (0, fp/2, Nf/2 + 1);

% wykres czesci rzeczywistej i urojonej
xr = real (widmo);
xi = imag (widmo);
subplot (312);
hold on;
plot (f, xr(1:Nf/2 + 1), 'g');
plot (f, xi(1:Nf/2 + 1), 'r');
xlabel ('czestotl.[Hz]');
ylabel ('re(X) & im(X)');

% wykres modulu i fazy widma
widmo_amp=abs(widmo);
widmo_faz=angle(widmo);
subplot(313);
plot (f, widmo_amp(1:Nf/2 + 1), 'k');
xlabel ('czestotl.[Hz]');
ylabel ('|X|');
figure(2)

plot (f, widmo_faz(1:Nf/2 + 1), 'k');
xlabel ('czestotl.[Hz]');
ylabel ('arg(x)');

set (gcf,'Position',[50 50 800 700]);

