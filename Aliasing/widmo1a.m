% skrypt wyznacza widmo amplitudowe i fazowe
% sygnalu bedacego suma dwoch sinusoid
clear all;
close all;
f1=20;	    % czestotliwosc pierwszej sinosoidy
A1=1;		% amplituda pierwszej sinusoidy
phi1=0.4;	% faza pierwszej sinusoidy

f2=70;	% czestotliwosc drugiej sinosoidy
A2=2;		% amplituda drugiej sinusoidy
phi2=0;		% faza drugiej sinusoidy

N=200;		% dlugosc sygnalu
fp=100;		% czestotliwosc probkowania
Nf=100;     % d�ugo�� transformacji Fouriera
% -------------------------------------------------------------------
%       W tych warunkach odleg�o�� pomi�dzy pr��kami widma wynosi 1Hz
% -------------------------------------------------------------------

% generuj os czasu
t=0:1/fp:(N-1)/fp;

% generuj sygnal
x=A1*sin(2*pi*f1*t+phi1)+A2*sin(2*pi*f2*t+phi2);
subplot (411);
plot (t,x);
xlabel ('czas[s]');
ylabel ('sygnal');

% wyznacz widmo
widmo=fft(x, Nf) / Nf;
f = linspace (0, fp/2, Nf/2 + 1);

% wykres czesci rzeczywistej i urojonej
xr = real (widmo);
subplot (412);
hold on;
stem (f, xr(1:Nf/2 + 1), 'g');
xi = imag (widmo);
stem (f, xi(1:Nf/2 + 1), 'r');
xlabel ('czestotl.[Hz]');
ylabel ('re(X) & im(X)');

% wykres modulu i fazy widma
widmo_amp=abs(widmo);
widmo_faz=angle(widmo);
subplot(413);
stem (f, widmo_amp(1:Nf/2 + 1), 'k');
xlabel ('czestotl.[Hz]');
ylabel ('|X|');

subplot(414);
stem (f, widmo_faz(1:Nf/2 + 1), 'b');
xlabel ('czestotl.[Hz]');
ylabel ('arg(X)');

set (gcf,'Position',[50 50 800 700]);