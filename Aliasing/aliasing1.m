clear all;
close all;
% ----------------  generowanie sinusa
fpx = 80;
Nx = 2000;
tx = 0:1/fpx:(Nx-1)/fpx;
f0 = 70;
f1 = 500;
x1= sin (2*pi*f0*tx) + 0.2*randn(1,Nx);

x = x1;
subplot (321);
plot (tx,x);
xlabel ('czas[s]');
ylabel ('sygnal przed decymacja');

Nf = 299;
wx = abs(fft(x,Nf));
fx = linspace(0, 300, Nf/2 + 1);
subplot (322);
plot (fx, wx(1:Nf/2 + 1));
xlabel ('czest[Hz]');
ylabel ('modul widma');

% ------------------------------------------------------------------
%      decymacja w petli
%      gdyz funkcja decimate przeprowadza filtracje antyaliasingową
% ------------------------------------------------------------------
dr = 2;
Ny = floor (Nx / dr); 
fpy = fpx / dr;
for i=1:Ny
  y(i) = x((i-1)*dr+1); 
end;    
ty = 0:1/fpy:(Ny-1)/fpy;
subplot (323);
plot (ty,y);
xlabel ('czas[s]');
ylabel ('sygnal po decymacji');

wy = abs(fft(y,Nf));
fy = linspace(0, fpy/2, Nf/2 + 1);
subplot (324);
plot (fy, wy(1:Nf/2 + 1));
xlabel ('czest[Hz]');
ylabel ('modul widma');

set (gcf,'Position',[50 50 800 700]);
%print -depsc ProbkAlias1

% decymacja sygnalu z filtrem antyaliasingowym

y = decimate(x,2); % dwukrotna decymacja sygnalu
fpy = fpx/2; % nowa czestotliwosc probkowania
Ny = floor (Nx / 2);

% sygnal po decymacji w dziedzinie czasu
ty = 0:1/fpy:(Ny-1)/fpy; % nowa os czasu
subplot(3,2,5)
plot(ty,y);
xlabel('czas [s]'); 
ylabel('sygnal po decymacji antyalias');

% sygnal po decymacji w dziedzinie czestotliwosci
Nfy = 2^11; % nowa dlugosc transformaty Fourier'a
yf = fft(y,Nfy); % policzenie transformaty Fourier'a
yf_mod = abs(yf); % policzenie modulu transformaty
fy = linspace(0,fpy/2,Nfy/2+1); % nowa os czestotliwosci
subplot(3,2,6)
plot(fy,yf_mod(1:Nfy/2+1));
xlabel('czestotliwosc [Hz]');
ylabel('modul widma antyalias');
