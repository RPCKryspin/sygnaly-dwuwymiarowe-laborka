% skrypt generuje spektrogramy sygnalu modelowego
% w postaci szumu bialego i sygnalu chirp oraz sygnalu
% mowy
clear all;
close all;
[x ,fpx] =audioread('mbi04dzba.wav');
% generacja sygnalu modelowego
N = 5000;
fp = 2000;
t = 0:1/fp:(N-1)/fp;
y = chirp (t, 100, 2.5, 900, 'q');

tx = 0:1/fpx:(length(x)-1)/fpx;

% narysowanie wykresow czasowych
subplot(221);
plot(tx,x);
xlabel('czas [s]');
ylabel('amplituda)');

% generacja spektrogramow
subplot (222);
spectrogram (x, 512, 256, 512, fpx);
subplot (223);
spectrogram(x,128,120,128,fpx);
subplot (224);
spectrogram(x,1280,1200,1208,fpx);

set (gcf,'Position',[50 50 1200 700]);