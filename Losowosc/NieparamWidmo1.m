% ############################################################
% ################### NIEPARAMETRYCZNE METODY ESTYMACJI WIDMA
clear all;
close all;
fp = 100;          % czestotliwo�� pr�bkowania
N = 100;            % d�ugo�� sugna�u 
N = 200;
N = 1000;
[x,tx] = sygnTestEstymWidma (fp, N);
subplot (221);
plot (tx ,x);
xlabel ('czas [s]');
ylabel ('sygnal');


% ------------- periodogram
Nf = 2^13;
N21 = Nf/2 + 1;
[Pxx1, f] = periodogram (x, [], Nf, fp);
subplot (222);
semilogy (f, Pxx1(1:N21));
ylim ([0.0001 100]);
legend ('periodogram');
xlabel ('czest [Hz]');
ylabel ('widm.gest.mocy');
title('Kwadrat modu�u widma Fouriera bez okienkowania');

% ------------- periodogram z oknem - widmo Cooleya
w = blackmanharris (N);
[Pxx2, f] = periodogram (x, w, Nf, fp);
subplot (223);
semilogy (f, Pxx2(1:N21));
ylim ([0.0001 100]);
legend ('periodogram z oknem blackmanharris');
xlabel ('czest [Hz]');
ylabel ('widm.gest.mocy');
title('Kw. mod. widma Four. z okienkowaniem');

% ------------- wgm z definicji
R = xcorr (x,x,N/2)/N;
w = blackmanharris (N+1);
R = R .* w';
Pxx3 = abs(fft(R,Nf))/sqrt(Nf);
subplot (224);
semilogy (f, Pxx3(1:N21));
ylim ([0.0001 100]);
legend ('widmo blackmana');
xlabel ('czest [Hz]');
ylabel ('widm.gest.mocy');
title('Transformata Fouriera okienkowanej funkcji autokorelacji');

set (gcf,'Position',[50 50 800 700]);
