function [x,t] = sygnEmit (fp, dlugC, f1, f2)
% -------------------------------------------------------------------------
% --- fp - cz�stotliwo�� pr�bkowania
% --- dlugC - d�ugo�� sygna�u emitowanego - d�ugo�� sygna�u chirp
% --- f1 - dolna czest. graniczna widma
% --- f2 - g�rna czest. graniczna widma

% --------  sygna� emitowany, z wybiegiem z przodu i z ty�u
Nc = floor(dlugC*fp); 
tc = 0:1/fp:(Nc-1)/fp;
x = chirp(tc,f1,dlugC,f2);
dlugP = 10^-6;
Np = floor(dlugP*fp); 
p = zeros(1, Np);
x = [p, x];
dlugK = 5*10^-6;
Nk = floor(dlugK*fp); 
p = zeros(1, Nk);
x = [x, p];
% --------- o� czasu dla wykresu
N = Np + Nc + Nk;
t=0:1/fp:(N-1)/fp;
