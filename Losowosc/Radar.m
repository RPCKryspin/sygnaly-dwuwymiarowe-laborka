% ############################################################
% ################### model prostego systemu radarowego
clear all;
close all;
fp = 1*10^9;          % czestotliwo�� pr�bkowania
dlugC = 10^-5;        % d�ugo�� sugna�u emitowanego przez radar - 10us
f1 = 0.4*10^9;        % dolna cz�st. graniczna widma
f2 = 0.45*10^9;       % g�rna czest. graniczna widma
[x,tx] = sygnEmit (fp, dlugC, f1, f2);
tx = 10^6 * tx;       % czas w us (mikrosekundach) 

figure (1);
subplot (221);
plot (tx ,x);
xlabel ('czas[us]');
ylabel ('sygn.emit');

% ------------- widmo sygna�u emitowanego
Nx = length (x);
Nf = 2^16;
N21 = Nf/2 + 1;
wx = abs(fft(x,Nf));
f = linspace (0,(fp/2)/(10^6),N21);  % os czestotliwosci w MHz
subplot (222);
plot (f, wx(1:N21));
xlabel ('czest[MHz]');
ylabel ('widmo sygn.emit');

% -------------  dekl. parametr�w odbi� i szumu, modelowanie odbicia
t1 = 11*10^-6;        % op�nienie 1.go odbicia - 1us
a1 = 0.003;            % mno�nik 1.go odbicia
t2 = 11.3*10^-6;      % op�nienie 2.go odbicia - 2us
a2 = 0.0015;           % mno�nik 2.go odbicia
b = 0.01;             % mnoznik szumu
odleglosc1 = t1 * 3*10^8 ;    % 3*10^8 to predko�� �wiat�a
odleglosc2 = t2 * 3*10^8;
[y,ty] = sygnOdbity (x, fp, t1, a1, t2, a2, b); 
ty = 10^6 * ty;
subplot (223);
plot (ty, y);
xlabel ('czas[us]');
ylabel ('sygn.odebrany');

% ------------- widmo sygna�u odebranego
wy = abs(fft(y,Nf));
subplot (224);
plot (f, wy(1:N21));
xlabel ('czest[MHz]');
ylabel ('widmo sygn.odebranego');
set (gcf,'Position',[50 300 700 500]);

% ---------------  korelacja wzajemna
tmax = 15*10^-6;         % maksymalne opo�nienie czasowe
kmax = floor (tmax*fp);
R = xcorr (x, y, kmax);
tR = -kmax/fp:1/fp:kmax/fp;
tR = 3*10^8 * tR;    % przeliczenie op�nienia na odleg�o��
figure (2);
plot (tR, R);
xlabel ('odleglosc [m]');
ylabel ('korel.wzajemna');
set (gcf,'Position',[700 50 700 500]);