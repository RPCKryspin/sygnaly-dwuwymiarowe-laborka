% ############################################################
% ################### PARAMETRYCZNE METODY ESTYMACJI WIDMA
clear all;
close all;
fp = 1000;          % czestotliwość próbkowania
N = 100;            % długość sugnału 
N = 200;
N = 1000;
[x,tx] = sygnTestEstymWidma (fp, N);
subplot (221);
plot (tx ,x);
xlabel ('czas [s]');
ylabel ('sygnal');

% ------------- periodogram
Nf = 2^13;
N21 = Nf/2 + 1;
p = 6;             %  długość filtru prognozującego
p = 12;
p = 24;
[Pxx1, f] = pcov (x, p, Nf, fp);
subplot (222);
semilogy (f, Pxx1(1:N21));
ylim ([0.0001 100]);
xlabel ('czest [Hz]');
ylabel ('widm.gest.mocy');
title('metoda kowariancyjna ');

% ------------- periodogram z oknem - widmo Cooleya
[Pxx2, f] = pburg (x, p, Nf, fp);
subplot (223);
semilogy (f, Pxx2(1:N21));
ylim ([0.0001 100]);
xlabel ('czest [Hz]');
ylabel ('widm.gest.mocy');
title('metoda Burga ');

% ------------- wgm z definicji
[Pxx3, f] = pyulear (x, p, Nf, fp);
subplot (224);
semilogy (f, Pxx3(1:N21));
ylim ([0.0001 100]);
xlabel ('czest [Hz]');
ylabel ('widm.gest.mocy');
title('metoda Yule-Walkera ');

set (gcf,'Position',[50 50 800 700]);