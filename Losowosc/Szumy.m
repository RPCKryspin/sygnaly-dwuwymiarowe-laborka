% -------------------------------------------------
% ----  LOSOWO��
% ----  KORELACJA WZAJEMNA - NIESKORELOWANIE

% ----  wczytanie sygnalu WAV nr 1
clear all;
close all;
[a, fa] = audioread ('kos_60_wr.wav');
dr = 12;
x = decimate (a,dr);
fp = fa/dr;
Nx=length(x);
t = 0:1/fp:(Nx-1)/fp;
subplot (421);
plot (t,x,'g');
xlabel ('czas [s]');
ylabel ('wartosc chwilowa');
% ----  wczytanie sygnalu WAV nr 2
[a, fa] = audioread ('aut_60_wr.wav');
dr = 12;
y = decimate (a,dr);
fp = fa/dr;
Ny=length(y);
t = 0:1/fp:(Ny-1)/fp;
subplot (422);
plot (t,y,'g');
xlabel ('czas [s]');
ylabel ('wartosc chwilowa');

% ----  estymacje autokorelacji i wykresy
Np = 1000;
N = 5000;
%N = 50000;
kmax = 600;
rx = xcorr (x(Np:Np+N), x(Np:Np+N), kmax);
tr = -kmax:1:kmax;
subplot (423);
plot (tr,rx/rx(kmax+1));
xlabel ('opoznienie [pr]');
ylabel ('autokorelacja x');

ry = xcorr (y(Np:Np+N), y(Np:Np+N), kmax);
subplot (424);
plot (tr,ry/ry(kmax+1));
xlabel ('opoznienie [pr]');
ylabel ('autokorelacja y');

% ---- estymacja widmowych gestosci mocy i wykresy
Nf = 2^10;
N21 = Nf/2+1;
sx = abs(fft(rx,Nf));
f = linspace (0, 0.5*fp, N21);
subplot (425);
plot (f,sx(1:N21));
xlabel ('czestotliwosc [Hz]');
ylabel ('widmowa gestosc mocy');

sy = abs(fft(ry,Nf));
subplot (426);
plot (f,sy(1:N21));
xlabel ('czestotliwosc [Hz]');
ylabel ('widmowa gestosc mocy');

% ---- estymacja korelacji wzajemnej i wykres
rxy = xcorr (x(Np:Np+N), y(Np:Np+N), kmax);
subplot (414);
plot (tr,rxy/sqrt(rx(kmax+1)*ry(kmax+1)));
xlabel ('opoznienie [pr]');
ylabel ('korelacja wzajemna');

set (gcf,'Position',[50 50 800 700]);

