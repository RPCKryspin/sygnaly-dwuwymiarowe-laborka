% -------------------------------------------------
% ----  WP�YW FILTRACJI SYGNA�U NA WYNIKI ESTYMACJI

% ----  wczytanie sygnalu WAV
clear all;
close all;
[a, fa] = audioread ('aut_60_wr.wav');
dr = 12;
x = decimate (a,dr);
fp = fa/dr;
Nx=length(x);
t = 0:1/fp:(Nx-1)/fp;
subplot (521);
plot (t,x);
xlabel ('czas [s]');
ylabel ('sygnal przed fltr');

% ----  projekt filtru i filtracja
M = 301;
h = fir1 (M-1, 0.2);
y = filter (h, 1, x);
subplot (522);
plot (t,y);
xlabel ('czas [s]');
ylabel ('sygnal po fltr');

% ----  estymacja autokorelacji i wykresy
kmax = 400;
Np = 1000;
N = 500;
N = 50000;
rx = xcorr (x(Np:Np+N), x(Np:Np+N), kmax);
tr = -kmax/fp:1/fp:kmax/fp;
subplot (523);
plot (tr,rx);
xlabel ('opoznienie [s]');
ylabel ('autokorelacja x');

ry = xcorr (y(Np:Np+N), y(Np:Np+N), kmax);
subplot (524);
plot (tr,ry);
xlabel ('opoznienie [s]');
ylabel ('autokorelacja y');

% ---- estymacja widmowej gestosci mocy i wykresy
Nf = 2^10;
N21 = Nf/2+1;
sx = abs(fft(rx,Nf));
f = linspace (0, 0.5*fp, N21);
subplot (525);
plot (f,sx(1:N21));
xlabel ('czestotliwosc [Hz]');
ylabel ('widmowa gestosc mocy');

sy = abs(fft(ry,Nf));
f = linspace (0, 0.5*fp, N21);
subplot (526);
plot (f,sy(1:N21));
xlabel ('czestotliwosc [Hz]');
ylabel ('widmowa gestosc mocy');

% ---- estymacja rozkladu prawdopodobienstwa i wykresy
xa = -0.05:0.002:0.05;
[nx ,xout] = hist (x(Np:Np+N),xa);
subplot (527);
plot (xout,nx/N);
xlabel ('wartosc chwilowa');
ylabel ('prawdopodobienstwo');

[ny ,xout] = hist (y(Np:Np+N),xa);
subplot (528);
plot (xout,ny/N);
xlabel ('wartosc chwilowa');
ylabel ('prawdopodobienstwo');

% --- estymacja korelacji wzajemnej unormowanej i wykres
rxy = xcorr (x(Np:Np+N), y(Np:Np+N), kmax);
rxy_norm = rxy / sqrt(rx(kmax+1)*ry(kmax+1));
subplot (515);
plot (tr,rxy_norm);
xlabel ('opoznienie [s]');
ylabel ('unorm.korelacja wzajemn.');

set (gcf,'Position',[50 50 800 700]);

