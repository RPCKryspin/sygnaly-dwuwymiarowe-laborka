% -------------------------------------------------
% ----  LOSOWO��
% ----  WP�YW D�UGO�CI SYGNA�U NA WYNIK ESTYMACJI

% ----  wczytanie sygnalu WAV
clear all;
close all;
[a ,fa] = audioread ('kos_60_wr.wav');
dr = 12;
x = decimate (a,dr);
fp = fa/dr;
N=length(x);
t = 0:1/fp:(N-1)/fp;
subplot (221);
plot (t,x,'g');
xlabel ('czas [s]');
ylabel ('wartosc chwilowa');

% ----  estymacja autokorelacji i wykresy
kmax = 100;
kmax = 800;
r1 = xcorr (x(1000:2000), x(1000:2000), kmax);
r2 = xcorr (x(1000:11000), x(1000:11000), kmax);
r3 = xcorr (x(1000:101000), x(1000:101000), kmax);
tr = -kmax:1:kmax;
subplot (222);
plot (tr,r1/r1(kmax+1),'r',tr,r2/r2(kmax+1),'g',tr,r3/r3(kmax+1),'b');
legend ('1000 pr', '10000 pr', '100000 pr');
xlabel ('opoznienie [pr]');
ylabel ('autokorelacja');

% ---- estymacja widmowej gestosci mocy i wykresy
Nf = 2^10;
N21 = Nf/2+1;
s1 = abs(fft(r1/1000,Nf));
s2 = abs(fft(r2/10000,Nf));
s3 = abs(fft(r3/100000,Nf));
f = linspace (0, 0.5*fp, N21);
subplot (223);
plot (f,s1(1:N21),'r',f,s2(1:N21),'g',f,s3(1:N21),'b');
%ylim ([0 6]);
xlabel ('czestotliwosc [Hz]');
ylabel ('widmowa gestosc mocy');

% ---- estymacja rozkladu prawdopodobienstwa i wykresy
xa = -0.1:0.002:0.1;
[nx, xout] = hist (x(1000:2000),xa);
[ny ,xout] = hist (x(1000:11000),xa);
[nz ,xout] = hist (x(1000:101000),xa);
subplot (224);
plot (xout,nx/1000,'r',xout,ny/10000,'g',xout,nz/100000,'b');
xlabel ('wartosc chwilowa');
ylabel ('prawdopodobienstwo');

set (gcf,'Position',[50 50 800 700]);

