% -------------------------------------------------
% ----  LOSOWO��
% ----  WP�YW D�UGO�CI SYGNA�U NA WYNIK ESTYMACJI
clear all;
close all;
%N = 100;
N = 10000;
% ----  generowanie i wykresy sygnalow
t = 0:1:N-1;
x = sin(2*pi*0.0731*t+2*pi*rand(1,1));
y = sin(2*pi*0.0731*t+2*pi*rand(1,1));
z = sin(2*pi*0.0731*t+2*pi*rand(1,1));
t = 0:1:(N-1);
subplot (221);
plot (t,x,'r',t,y,'g',t,z,'b');
xlabel ('czas [pr]');
ylabel ('wartosc chwilowa');

% ----  estymacja autokorelacji i wykresy
kmax = 100;
rx = xcorr (x, x, kmax);
ry = xcorr (y, y, kmax);
rz = xcorr (z, z, kmax);
tr = -kmax:1:kmax;
subplot (222);
plot (tr,rx,'r',tr,ry,'g',tr,rz,'b');
xlabel ('opoznienie [pr]');
ylabel ('autokorelacja');

% ---- estymacja widmowej gestosci mocy i wykresy
Nf = 256;
N21 = Nf/2+1;
sx = abs(fft(rx/N,Nf));
sy = abs(fft(ry/N,Nf));
sz = abs(fft(rz/N,Nf));
f = linspace (0, 0.5, N21);
subplot (223);
plot (f,sx(1:N21),'r',f,sy(1:N21),'g',f,sz(1:N21),'b');
xlabel ('czestotliwosc unormowana');
ylabel ('widmowa gestosc mocy');

% ---- estymacja rozkladu prawdopodobienstwa i wykresy
xa = -1.0:0.02:1.0;
[nx xout] = hist (x,xa);
[ny xout] = hist (y,xa);
[nz xout] = hist (z,xa);
subplot (224);
plot (xout,nx/N,'r',xout,ny/N,'g',xout,nz/N,'b');
xlabel ('wartosc chwilowa');
ylabel ('prawdopodobienstwo');

set (gcf,'Position',[50 50 800 700]);

