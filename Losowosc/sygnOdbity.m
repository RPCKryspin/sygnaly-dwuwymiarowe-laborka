function [y,ty] = sygnOdbity (x, fp, t1, a1, t2, a2, b)
% -------------------------------------------------------------------------
% --- fp - cz�stotliwo�� pr�bkowania
% --- t1 - op�znienie 1.go odbicia
% --- a1 - amplituda 1.go odbicia
% --- t2 - op�nienie 2.go odbicia
% --- a2 - amplituda 2.go odbicia
% --- b - mno�nik szumu addytywnego

% -------------------------------------------------------------------------
% UWAGI:
% -------------------------------------------------------------------------

% ------ dlug sygnalu emitowanego
Nx = length (x); 
Ny = 2^16;
y = zeros (1, Ny);
% -------  pierwsze odbicie
N1 = floor(t1*fp);   % op�nienie w pr�bkach
p = zeros(1,N1);
x1 = [p, x];
x1 = a1 * x1;
N1 = length (x1);
p = zeros (1, Ny-N1);
x1 = [x1, p];        % zr�wnanie d�ugo�ci wektor�w y i x1
y = y + x1;

% -------  drugie odbicie
N2 = floor(t2*fp);          % opo�nienie w probkach
p = zeros(1, N2);
x2 = [p, x];
x2 = a2 * x2;
N2 = length (x2);
p = zeros (1, Ny-N2);
x2 = [x2, p];        % zr�wnanie d�ugo�ci wektor�w y i x2
y = y + x2;

% -------  szum gaussowski
szum = randn (1, Ny);
y = y + b*szum;

% -------  generowanie osi czasu do wykresu sygna�u
ty=0:1/fp:(Ny-1)/fp;
