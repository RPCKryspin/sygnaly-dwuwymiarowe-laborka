function [x,t] = sygnTestEstymWidma (fp, N)

f1=0.10*fp;
f2=0.11*fp;
f3=0.20*fp;
A1=1;
A2=1;
A3=0.2;
An=0.2;
t=0:1/fp:(N-1)/fp;
x = A1*sin(2*pi*f1*t) + A2*sin(2*pi*f2*t) + A3*sin(2*pi*f3*t) + An*randn(1,N);