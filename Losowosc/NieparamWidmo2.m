% ############################################################
% ################### NIEPARAMETRYCZNE METODY ESTYMACJI WIDMA
clear all;
close all;
[a ,fa] = audioread ('aut_60_wr.wav');
dr = 12;
x = decimate (a,dr);
fp = fa/dr;
Nx=length(x);
Np = 10000;
N = 50;
N = 200;
N = 1000;
tx = 0:1/fp:(Nx-1)/fp;
subplot (221);
plot (tx(Np:Np+N) ,x(Np:Np+N));
xlabel ('czas [s]');
ylabel ('sygnal');

% ------------- periodogram
Nf = 2^14;
N21 = Nf/2 + 1;
[Pxx1, f] = periodogram (x(Np:Np+N-1), [], Nf, fp);
subplot (222);
semilogy (f, Pxx1(1:N21));
ylim ([10^-9 10^-5]);
legend ('Brak okienkowania');
xlabel ('czest [Hz]');
ylabel ('widm.gest.mocy');

% ------------- periodogram z oknem - widmo Cooleya
z = x(Np:Np+N-1);
w = blackmanharris (N);
[Pxx2, f] = periodogram (z, w, Nf, fp);
subplot (223);
semilogy (f, Pxx2(1:N21));
ylim ([10^-9 10^-5]);
legend ('widmo Cooleya');
xlabel ('czest [Hz]');
ylabel ('widm.gest.mocy');

% ------------- wgm z definicji
R = xcorr (x(Np:Np+N), x(Np:Np+N), N/2) / N;
w = blackmanharris (N+1);
R = R .* w;
Pxx3 = abs(fft(R,Nf))/sqrt(Nf);
subplot (224);
semilogy (f, Pxx3(1:N21));
ylim ([10^-9 10^-5]);
legend ('widmo blackmana');
xlabel ('czest [Hz]');
ylabel ('widm.gest.mocy');

set (gcf,'Position',[50 50 800 700]);
